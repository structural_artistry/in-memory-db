require 'readline'
load 'lib/in_memory_db.rb'
prompt = "> "

db = InMemoryDbLib.new

def print_instructions
  puts <<~HEREDOC
    The In-Memory DB takes the following commands and syntax:
    SET key value  # write a value
    GET key        # return a stored value
    DELETE key     # delete a stored value
    COUNT value    # get incidence count of a value
    BEGIN          # begin a new transaction
    ROLLBACK       # rollback the most recent transaction
    COMMIT         # commit all outstanding transactions
    END            # exit the program... all data will be lost so choose wisely
  HEREDOC
end

while buf = Readline.readline(prompt, true)
  begin
    args = buf.split(/ /)
    case args[0]
    when 'GET'
      if args.size != 2
        print_instructions
      else
        result = db.get args[1]
        if result == nil
          puts 'NULL'
        else
          puts result
        end
      end
    when 'SET'
      if args.size != 3
        print_instructions
      else
        db.set args[1], args[2]
      end
    when 'COUNT'
      if args.size != 2
        print_instructions
      else
        puts db.count args[1]
      end
    when 'BEGIN'
      if args.size != 1
        print_instructions
      else
        db.begin_transaction
      end
    when 'ROLLBACK'
      if args.size != 1
        print_instructions
      else
        result = db.rollback
        puts result if ![true, nil].include?(result)
      end
    when 'COMMIT'
      if args.size != 1
        print_instructions
      else
        db.commit
      end
    when 'DELETE'
      if args.size != 2
        print_instructions
      else
        db.delete args[1]
      end
    when 'END'
      exit!
    else
      print_instructions
    end
  rescue Exception => ex
    puts "ERROR: An unhandled error occurred, details: #{ex}"
    print_instructions
  end
end

