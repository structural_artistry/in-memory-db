require 'test/unit'
require_relative '../lib/in_memory_db.rb'

class InMemoryDbTest  < Test::Unit::TestCase
  def setup
    @db = InMemoryDbLib.new
  end

  def test_transaction_not_found
    @db.begin_transaction
    @db.set 'me', 'alexi'
    @db.begin_transaction
    @db.set 'me', 'someoneelse'
    assert_equal @db.get('me'), 'someoneelse'
    @db.rollback
    assert_equal @db.get('me'), 'alexi'
    @db.rollback
    assert_equal @db.get('me'), nil
    assert_equal @db.rollback, 'TRANSACTION NOT FOUND'
  end

  def test_example_1
    assert_equal @db.count('foo'), 0
    assert_equal @db.get('a'), nil
    @db.set('a', 'foo')
    assert_equal @db.get('a'), 'foo'
    @db.set('b', 'foo')
    assert_equal @db.count('foo'), 2
    assert_equal @db.count('bar'), 0
    @db.delete('a')
    assert_equal @db.count('foo'), 1
    @db.set('b', 'baz')
    assert_equal @db.count('foo'), 0
    assert_equal @db.get('b'), 'baz'
    assert_equal @db.get('B'), nil
  end

  def test_example_2
    @db.set('a', 'foo')
    @db.set('a', 'foo')
    assert_equal @db.count('foo'), 1
    assert_equal @db.get('a'), 'foo'
    @db.delete('a')
    assert_equal @db.get('a'), nil
    assert_equal @db.count('foo'), 0
  end

  def test_example_3
    assert_equal @db.begin_transaction, true
    assert_equal @db.get('a'), nil
    assert_equal @db.transaction_buffer, [[:begin_transaction]]
    @db.set('a', 'foo')
    assert_equal @db.transaction_buffer, [[:begin_transaction],[:delete,'a']]
    assert_equal @db.get('a'), 'foo'
    assert_equal @db.begin_transaction, true
    assert_equal @db.transaction_buffer, [[:begin_transaction],[:delete,'a'],[:begin_transaction]]
    @db.set('a', 'bar')
    assert_equal @db.transaction_buffer, [[:begin_transaction],[:delete,'a'],[:begin_transaction],[:set,'a','foo']]
    assert_equal @db.get('a'), 'bar'
    @db.set('a', 'baz')
    assert_equal @db.get('a'), 'baz'
    assert_equal @db.transaction_buffer, [[:begin_transaction],[:delete,'a'],[:begin_transaction],[:set,'a','foo'],[:set,'a','bar']]
    @db.rollback
    assert_equal @db.transaction_buffer, [[:begin_transaction],[:delete,'a']]
    assert_equal @db.get('a'), 'foo'
    @db.rollback
    assert_equal @db.get('a'), nil
  end

  def test_example_4
    @db.set('a', 'foo')
    @db.set('b', 'baz')
    @db.begin_transaction
    assert_equal @db.get('a'), 'foo'
    @db.set('a', 'bar')
    assert_equal @db.count('bar'), 1
    @db.begin_transaction
    assert_equal @db.count('bar'), 1
    @db.delete('a')
    assert_equal @db.get('a'), nil
    assert_equal @db.count('bar'), 0
    @db.rollback
    assert_equal @db.get('a'), 'bar'
    assert_equal @db.count('bar'), 1
    @db.commit
    assert_equal @db.get('a'), 'bar'
    assert_equal @db.get('b'), 'baz'
  end

  # don't know that this is actually testing the size as the number does not increase at all up to 1m+
  def test_size
    require 'objspace'
    mem_sizes = []
    1000.times do |n|
      @db.set(n, "foo#{n}")
      @db.set((n+100), "bar#{n}")
      mem_sizes << ObjectSpace.memsize_of(@db)
    end
    assert_equal mem_sizes.uniq, [40]
  end

end
