class InMemoryDbLib
  attr_reader :transaction_buffer

  def initialize
    @store = {}
    @transaction_buffer = []
    @transaction_enabled = false
  end

  def begin_transaction
    @transaction_buffer << [:begin_transaction]
    @transaction_enabled = true
  end

  def rollback
    return 'TRANSACTION NOT FOUND' if @transaction_buffer.size == 0

    # disable transaction so we don't record the rollback actions in the buffer
    @transaction_enabled = false
    # iterate transaction buffer replaying back to last :begin_transaction
    @transaction_buffer.reverse.each do |item|
      action, key, value = item
      case action
      when :set
        set(key, value)
      when :delete
        delete(key)
      end
      # remove the last item corresponding to our reverse iteration
      @transaction_buffer.pop

      # we only roll back to the previous transaction start
      break if action == :begin_transaction
      nil
    end

    # turn transaction enabled back on if there are still items in the buffer
    @transaction_enabled = true if @transaction_buffer.size > 0
  end

  def commit
    @transaction_buffer = []
    @transaction_enabled = false
  end

  def set(key, value)
    manage_transaction_buffer(key)
    @store[key] = value
  end

  def get(key)
    @store[key]
  end

  def delete(key)
    manage_transaction_buffer(key)
    @store.delete(key)
  end

  def count(value)
    count = 0
    @store.each{|k,v| count+=1 if v==value }
    count
  end

  private

  def manage_transaction_buffer(key)
    if @transaction_enabled
      existing_value = get(key)
      if existing_value
        # the old value to replay to undo...
        @transaction_buffer << [:set, key, existing_value]
      else
        # no existing value so rollback should delete it
        @transaction_buffer << [:delete, key]
      end
    end
  end

end
